clear; clc;

%% Import data
sampling_period = 1;
filename = 'data/output.csv';
[FhM,NoR,MCA,RfM,HfM,VP2,I1,I2,I3,I4] = importfile(filename);

data = iddata([I1,I2,I3,I4],[FhM,NoR,MCA,RfM,HfM,VP2], sampling_period);
data.InputName  = {'FhM';'NoR';'MCA'; 'RfM'; 'HfM'; 'VP2'};
data.OutputName = {'I1'; 'I2'; 'I3';'I4'};

%% Identify model
comp = 1;   % Compares the data with the model
[A,B,C,D] = identify_model(data,sampling_period,comp);
sys = ss(A,B,C,D,sampling_period);

% Saving data dimensions
n = size(A,1);
m = size(B,2);
p = size(C,1);

%% Control parameters
L = 3;
q_vec = [5,10,10,1];   if length(q_vec) ~= p, error('q_vec must be of length p'); end
r_vec = [1,1,1,1,1,1]; if length(r_vec) ~= m, error('r_vec must be of length m'); end
Q = diag(repmat(q_vec,1,L));
R = diag(repmat(r_vec,1,L));

Umin = [30,1,0,0,0,0]';      if length(Umin) ~= m, error('Umin must be of length m'); end
Umax = [100,5,100,40,30,1]'; if length(Umax) ~= m, error('Umax must be of length m'); end
DeltaUmin = Umin - Umax;
DeltaUmax = Umax - Umin;

%% Kalman filter design
Qn = 1e0 * eye(m,m);
Rn = 1e0 * eye(p,p);

[kal,Lk,Pk] = kalman(sys,Qn,Rn);

%% Setpoint
sp = [80,100,100,80]';

%% Write needed matrices in Python
out = write_python_vec(A,B,C,D,L,q_vec,r_vec,Umin,Umax,DeltaUmin,DeltaUmax,diag(Qn)',diag(Rn)',Lk,Pk,sp);
fprintf(out);


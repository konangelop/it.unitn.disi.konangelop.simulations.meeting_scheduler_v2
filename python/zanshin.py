import numpy as np
import argparse
import os
import sys
import csv

from multiprocessing.connection import Listener
from array import array

#from numpy import *

# My libraries
import libs.mpyc as reg
import libs.utils as ut


I1o = 80
I2o = 100
I3o = 100
I4o = 80

lastTreatedI1=False
lastTreatedI2=False
lastTreatedI3=False
lastTreatedI4=False

y = [0.0,0.0,0.0,0.0]
u = [60,0,0,0,0,0]

needCorrection=[0,0,0,0]

def increaseFhM(x):
    global u
    u[0]=u[0]+x
    u[0]=max(min(u[0],100),40)

def decreaseFhM(x):
    global u
    u[0]=u[0]-x
    u[0]=max(min(u[0],100),40)


def increaseNoR(x):
    global u
    u[1]=u[1]+x
    u[1]=max(min(u[1],5),1)

def decreaseNoR(x):
    global u
    u[1]=u[1]-x
    u[1]=max(min(u[1],5),1)


def increaseMCA(x):
    global u
    u[2]=u[2]+x
    u[2]=max(min(u[2],100),0)


def decreaseMCA(x):
    global u
    u[2]=u[2]-x
    u[2]=max(min(u[2],100),0)


def increaseRfM(x):
    global u
    u[3]=u[3]+x
    u[3]=max(min(u[3],40),0)


def decreaseRfM(x):
    global u
    u[3]=u[3]-x
    u[3]=max(min(u[3],40),0)

def increaseHfM(x):
    global u
    u[4]=u[4]+x
    u[4]=max(min(u[4],30),0)


def decreaseHfM(x):
    global u
    u[4]=u[4]-x
    u[4]=max(min(u[4],40),0)

def increaseVP2(x):
    global u
    u[5]=u[5]+x
    u[5]=max(min(u[5],1),0)


def decreaseVP2(x):
    global u
    u[5]=u[5]-x
    u[5]=max(min(u[5],1),0)


def correctI1():
    rand = np.random.randint(1,100)
    if(rand<40):
        decreaseHfM(1)
    else:
        increaseVP2(1)

def correctI2():
    rand =np.random.randint(1,100)
    if(rand<70):
        increaseRfM(5)
    else:
        increaseHfM(3)

def correctI3():
    rand = np.random.randint(1,2)
    if(rand==1):
        decreaseFhM(10)
    else:
        increaseMCA(10)

def correctI4():
    rand = np.random.randint(1,3)
    if(rand==1):
        decreaseMCA(10)
    elif(rand==2):
        increaseFhM(20)
    else:
        increaseNoR(1)


def main():
    global u, needCorrection,lastTreatedI1,lastTreatedI2,lastTreatedI3,lastTreatedI4
    address = ('localhost', 6000)     # family is deduced to be 'AF_INET'
    listener = Listener(address, authkey='secret password')
    print "Waiting for connections..."
    conn = listener.accept()
    print "Connection established!"

    while True:
        print "Waiting for data..."
        msg = conn.recv()
        print "Data received!"

        # do something with msg
        if msg == 'close':
            conn.close()
            break
        else:
            #control_input = [float(i) for i in msg[0]]
            y = msg
            print 'received output:', y

            #check for corrections

            if(I2o-y[1]>0 and not lastTreatedI2):
                print 'correcting I2'
                correctI2()
                lastTreatedI2=True
                lastTreatedI1=False
                lastTreatedI3=False
                lastTreatedI4=False
            elif(I3o-y[2]>0 and not lastTreatedI3):
                print'correcting I3'
                correctI3()
                lastTreatedI1=False
                lastTreatedI2=False
                lastTreatedI3=True
                lastTreatedI4=False
            elif(I4o-y[3]>0 and not lastTreatedI4):
                print 'correcting I4'
                correctI4
                lastTreatedI1=False
                lastTreatedI2=False
                lastTreatedI3=False
                lastTreatedI4=True
            elif(I1o-y[0]>0 and not lastTreatedI1):
                print 'correcting I1'
                correctI1()
                lastTreatedI1=True
                lastTreatedI2=False
                lastTreatedI3=False
                lastTreatedI4=False

            print 'sending control input: ',u
            conn.send(u)

    # Closing the listener
    listener.close()

    print '\nSimulation completed!\n'


if __name__ == "__main__":
    sys.exit(main())

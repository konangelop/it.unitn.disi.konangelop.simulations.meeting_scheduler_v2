#!/usr/bin/python

import sys
import numpy as np
import matplotlib.pyplot as plt
from pylab import *
import csv

import libs.utils as ut

from multiprocessing.connection import Client
from array import array
from subprocess import call


# Environmental parameters
punctuality = 0.84  # A factor to measure how punctual the participants
                    # are. The xrange of values is [0,1] low->high.
minNoM = 5
maxNoM = 10
availability = 0.79      # A factor to measure how available are the invited
                    # participants. xrange of values [0,1] low->high

minNoP = 12            #The minimum number of participants
maxNoP = 13           #The maximum number of participants
budget = 3500        #The daily amount of money disposed for meetings
dailyCost = [0,0,0,0,0,0,0,0]    #The daily amount of money spent
                                #for meetings

# Indicators
I1 = []  # Low Cost
I2 = []  # Find Room
I3 = []  # Find Date
I4 = []  # Good Participation
I5 = []  # Fast Scheduling
I6 = []  # Good Quality

# Control Parameters
MCA = 0 # Maximum conflicts allowed (% over the participants)
VP1 = 0  # 0 = by phone | 1 = e-mail | 2 = automatically
VP2 = 1 # 0 = listed   | 1 = suggested - find room
VP3 = 0  # 0 = manually | 1 = automatically - find date
FhM = 60  # From how many participants the timetables are
# collected
RfM = 0  # Local rooms for meetings
HfM = 0  # Hotel rooms for meetings
NoR = 0  # Number of reminders
NoS = 3  # Number of secretaries
VPA = 0  # View private apointmantes 0 = no | 1 = yes

MCA_log = []
NoR_log = []
FhM_log = []
RfM_log = []
HfM_log = []
VP2_log = []

P = 0   #Number of participants
M = 0

# Simulation Variables
totalMeetingsRequested = 0  #The total amount of meeting requests
totalRoomFound = 0  # The total number of meetings for which a room is
                    # successfully found
totalDateFound = 0  # The total number of meetings for which a date is
                    # is successfully found
totalGoodRooms=0    #Total fully equiped rooms found
totalGoodParticipation=0 #The number of meetings above the participation
                        #thresshold
totalDaysWithinBudget=0
#cheapHotelPrice = 30
#expensiveHotelPrice = 40
hotelPrice = 60
participationThr = 80    #The % of participants that must show up


dailyWorkload =  [0, 0, 0, 0, 0, 0, 0, 0]
roomFoundSlots = [0, 0, 0, 0, 0, 0, 0, 0]
dateFoundSlots = [0, 0, 0, 0, 0, 0, 0, 0]

SimulationLength = 60 # The days the simulated scheduler will run

controllerON = True

output = [I1,I2,I3,I4]


# Distributing the meeting requests to each of the 8 time slots of the
# day. Input is the number of meetings that must be scheduled.
def workloadDistribution(workload, day):
    global dailyWorkload
    dailyWorkload = workload[day]
    #dailyWorkload = np.random.randint(minimum, maximum, 8)
    #ut.logInput(dailyWorkload[0],dailyWorkload[1],dailyWorkload[2],dailyWorkload[3],dailyWorkload[4],dailyWorkload[5],dailyWorkload[6],dailyWorkload[7])
    #ut.logInput(dailyWorkload)

    return;

def resetWorkload():
    global dailyWorkload
    dailyWorkload = [0, 0, 0, 0, 0, 0, 0, 0]
    return;

def participationCheck():

    # if punctuality > 0.9 and availability > 0.9:
    #     participatedPr = availability*50 + NoR*5 + 0.4*FhM
    # elif punctuality <0.9:
    #     participatedPr = availability*50 + NoR*10 + 0.2*FhM
    # else:
    #     participatedPr = availability*70 + NoR*6 + 0.15*FhM
    #participatedPr = 50*(float(punctuality + availability) / 2.0) + 0.35*FhM + 15*NoR
    participatedPr = 0.794*FhM + 0.05006*punctuality + 0.02752*availability**2 + 295*float(NoR)/float(FhM+0.01) - 39.4 - 2.487*availability  - 0.9662*punctuality*NoR
    return participatedPr
    #print 'participation probability: ', participatedPr
    # rand=np.random.randint(1,100)
    # if rand <= participatedPr:
    #     #print '$$$'
    #     return True
    # else:
    #     return False

def calculateDailyCost():
    global dailyCost


    for i in xrange(0, 8):
        #print 'calculating cost for ',roomFoundSlots[i],' rooms'
        if(VP2 == 0):
            if(roomFoundSlots[i]>=HfM):
                dailyCost[i]=HfM*hotelPrice
            else:
                dailyCost[i]=roomFoundSlots[i]*hotelPrice
        else:
            if(roomFoundSlots[i]>RfM):
                dailyCost[i]=(roomFoundSlots[i]-RfM)*hotelPrice
            else:
                dailyCost[i]=0


#gets as input the number of participants and returns true if a date is found
def dateFound(n):
    contactedParticipants = n*FhM/100
    rand1 = np.random.randint(1,100)
    if rand1 < availability*100:
        rand2 = np.random.randint(1,100)
        pr = 7.371*FhM + 0.01321*FhM*MCA + 554.9*n/float(FhM+0.01) - 278.4 - 3.5*n - 0.01782*n*MCA - 0.03618*n**2 - 0.002603*n**2*FhM
        if (rand2 < 115 - 1.2*contactedParticipants + 1.8*contactedParticipants*MCA/100):
            return True
    else:
        return False

#Finds rooms and dates for all the meetings of a day
def bookMeetings():
    global dailyWorkload,totalDateFound,totalRoomFound,totalGoodParticipation
    global dateFoundSlots,roomFoundSlots

    #workloadDistribution(minNoM,maxNoM)
    #Finding Date
    dateFoundSlots = [0, 0, 0, 0, 0, 0, 0, 0]
    roomFoundSlots = [0, 0, 0, 0, 0, 0, 0, 0]
    participantsNumList = []
    #print 'workload: ', dailyWorkload
    for i in range(0,8):            #for every slot for meetings
        workload = dailyWorkload[i] # the workload of the spot
        for j in range(0,workload): # for every meeting try to book
            # The number of participants for this meeting
            participants = np.random.randint(minNoP,maxNoP)

            if(dateFound(participants)):
                participantsNumList.append(participants)
                totalDateFound+=1
                dateFoundSlots[i]+=1
    #print 'dates found: ', dateFoundSlots
    #Finding Room
    for k in range(0,8):
        roomWorkload = dateFoundSlots[k]
        participantsNumListFiltered = [] # the participants for every meeting that took actually place
        spareRooms=0
        if(roomWorkload<RfM+HfM):
            spareRooms=RfM + HfM -roomWorkload
        #for all the meeting requests in this slot
        rooms=RfM+HfM
        for m in range (0,roomWorkload):
            if(rooms<=0):
                break
            findRoomPr=100
            #when choosing from the list it's easier to find a room even oif it's not the best
            #so spare rooms have less contribution when VP = 0
            #when system suggests rooms it suggests only suitable rooms so spare rooms must be more
            if(VP2 == 0):
                findRoomPr = 100 - participantsNumList[m]*1.2 + spareRooms*1.8
            else:
                findRoomPr = 100 - participantsNumList[m]*1.2 + spareRooms*0.8
            rand = np.random.randint(1,100)
            #check if the room is found
            if (rand <= findRoomPr):
                roomFoundSlots[k]+=1
                totalRoomFound+=1
                rooms-=1
                #Claculating Participation
                count = 0
                #print 'participants: ',participantsNumList[m]
                # for z in range(0,participantsNumList[m]):
                #     #print 'z = ',z
                #     if(hasParticipated()):
                #         count = count + 1
                #print 'count = ', count, 'over ', participantsNumList[m]
                #print float(count)/float(participantsNumList[m])*100,'%'

                if(participationCheck() >= participationThr):
                    #print 'good participation'
                    totalGoodParticipation = totalGoodParticipation + 1
    #print 'rooms found: ', roomFoundSlots
    calculateDailyCost()
    #print 'daily cost: ', sum(dailyCost)

def calculateI(day):
    global budget,totalDaysWithinBudget,totalRoomFound,I1,I2,dailyCost
    global roomFoundSlots

    if sum(dailyCost)<=budget:
        totalDaysWithinBudget+=1
    I1.append(float(totalDaysWithinBudget)/float(day)*100)
    I2.append(float(totalRoomFound)/float(totalDateFound+1)*100)
    I3.append(float(totalDateFound)/float(totalMeetingsRequested)*100)
    participation = int((45*(availability)**2) +(10*punctuality)**2+ 0.45*FhM + 5*NoR - 0.3*MCA)
    if(participation>100):
    	participation=100

    #print 'Participation',participation,'%'
    I4.append(participation)
    FhM_log.append(FhM)
    NoR_log.append(NoR)
    MCA_log.append(MCA)
    RfM_log.append(RfM)
    HfM_log.append(HfM)
    VP2_log.append(VP2)

def sim_id(step):
    global NoR,RfM,HfM,MCA,FhM,VP2
    if(step==5):
        NoR+=1
    elif(step==10):
        NoR+=1
    elif(step==15):
        NoR-=1
    elif(step==30):
        NoR-=1
    elif(step==45):
        NoR=0
    elif(step==60):
        RfM+=2
    elif(step==75):
        RfM+=2
    elif(step==90):
        RfM+=5
    elif(step==100):
        RfM-=10
    elif(step==120):
        HfM+=5
    elif(step==150):
        HfM+=8
    elif(step==175):
        VP2=0
    elif(step==200):
        VP2=1
    elif(step==230):
        VP2=0
    elif(step==280):
        HfM-=8
    elif(step==330):
        RfM+=12
    elif(step==400):
        MCA=30
    elif(step==450):
        MCA=20
    elif(step==490):
        MCA=0
    elif(step==520):
        MCA=25
    elif(step==560):
        FhM=80
    elif(step==590):
        FhM=100
    elif(step==620):
        FhM=70
    elif(step==700):
        NoR=2
    elif(step==730):
        NoR=1
    elif(step==780):
        NoR=2
    elif(step==830):
        NoR=4
    elif(step==840):
        FhM=100

def sim_id2(step):
    global FhM,NoR,MCA,RfM,HfM,VP2
    if(step % 10 == 0):
        FhM= np.random.randint(30,100)
        NoR= np.random.randint(0,5)
        MCA= np.random.randint(0,100)
        RfM= np.random.randint(0,40)
        HfM= np.random.randint(0,30)
        VP2= np.random.randint(0,2)

def sim_id3(step):
    global FhM,NoR,MCA,RfM,HfM,VP2
    if(step % 30 == 0):
        coin1 = np.random.randint(1,100)
        coin2 = np.random.randint(1,100)
        coin3 = np.random.randint(1,100)
        coin4 = np.random.randint(1,100)
        coin5 = np.random.randint(1,100)
        coin6 = np.random.randint(1,100)
        if(coin1>50):
            if(FhM==100):
                FhM = 30
            else:
                FhM = 100
        if(coin2 > 50 ):
            if(NoR == 5):
                NoR = 1
            else:
                NoR = 5
        if(coin3 > 50):
            if(MCA == 100):
                MCA = 0
            else:
                MCA = 100
        if(coin4 > 50):
            if(RfM == 40):
                RfM = 0
            else:
                RfM = 40
        if(coin5 > 50):
            if(HfM == 30):
                HfM = 0
            else:
                HfM = 30
        if(coin6 > 50):
            if(VP2 == 1):
                VP2 = 0
            else:
                VP2 =1


def sim_scenario1c(step):
    global maxNoM,minNoM

    if(step==50):
        minNoM = 40
        maxNoM = 55
    elif(step==55):
        minNoM = 38
        maxNoM = 53
    elif(step==60):
        minNoM =34
        maxNoM =50
    elif(step==65):
        minNoM = 32
        maxNoM = 48
    elif(step == 70):
        minNoM = 28
        maxNoM = 34


    elif(step==90):
        minNoM = 20
        maxNoM = 25
    elif(step==120):
        minNoM = 40
        maxNoM = 55

    elif(step==125):
        minNoM = 38
        maxNoM = 53
    elif(step==130):
        minNoM =34
        maxNoM =50
    elif(step==135):
        minNoM = 32
        maxNoM = 48
    elif(step == 140):
        minNoM = 28
        maxNoM = 34

    elif(step==160):
        minNoM = 20
        maxNoM = 25
    elif(step==200):
        minNoM = 40
        maxNoM = 55

    elif(step==215):
        minNoM = 38
        maxNoM = 53
    elif(step==220):
        minNoM =34
        maxNoM =50
    elif(step==225):
        minNoM = 32
        maxNoM = 48
    elif(step == 230):
        minNoM = 28
        maxNoM = 34

    elif(step==260):
        minNoM = 20
        maxNoM = 25
    elif(step==290):
        minNoM = 40
        maxNoM = 55

    elif(step==300):
        minNoM = 38
        maxNoM = 53
    elif(step==310):
        minNoM =34
        maxNoM =50
    elif(step==320):
        minNoM = 32
        maxNoM = 48
    elif(step == 325):
        minNoM = 28
        maxNoM = 34

    elif(step==330):
        minNoM = 20
        maxNoM = 25
    elif(step==290):
        minNoM = 40
        maxNoM = 55
    elif(step==430):
        minNoM = 20
        maxNoM = 25

#workload variation
def sim_scenario1b(step):
    global maxNoM,minNoM,availability
    if(step == 200):
        minNoM = 30
        maxNoM = 35
        availability = 0.91
    elif(step == 210):
        minNoM = 35
        maxNoM = 40
        availability = 0.84
    elif(step == 220):
        minNoM = 40
        maxNoM = 45
        availability = 0.8
    elif(step == 230):
        minNoM = 50
        maxNoM = 55
        availabilty = 0.76
    elif(step == 240):
    	minNoM = 60
        maxNoM = 65
        availability = 0.7
    elif(step == 300):
    	minNoM=25
    	maxNoM=30
    elif(step == 310):
        minNoM = 30
        maxNoM = 35
        availability = 0.91
    elif(step == 320):
        minNoM = 45
        maxNoM = 50
        availability = 0.84
    elif(step == 330):
        minNoM = 55
        maxNoM = 60
        availability = 0.8
    elif(step == 340):
        minNoM = 60
        maxNoM = 65
        availability = 0.76
    elif(step == 350):
    	minNoM = 70
        maxNoM = 75
        availability = 0.7
    elif(step == 360):
    	minNoM=25
    	maxNoM=30
    elif(step == 400):
        minNoM = 30
        maxNoM = 35
        availability = 0.91
    elif(step == 410):
        minNoM = 35
        maxNoM = 40
        availability = 0.84
    elif(step == 420):
        minNoM = 45
        maxNoM = 50
        availability = 0.8
    elif(step == 430):
        minNoM = 50
        maxNoM = 55
        availability = 0.76
    elif(step == 440):
    	minNoM = 60
        maxNoM = 65
        availability = 0.7
    elif(step == 400):
    	minNoM=25
    	maxNoM=30

def sim_scenario1d(step):
    global maxNoM,minNoM,availability
    if(step == 3):
        minNoM = 30
        maxNoM = 35
        availability = 0.91
    elif(step == 6):
        minNoM = 35
        maxNoM = 40
        availability = 0.84
    elif(step == 9):
        minNoM = 40
        maxNoM = 45
        availability = 0.8
    elif(step == 12):
        minNoM = 50
        maxNoM = 55
        availabilty = 0.76
    elif(step == 13):
        minNoM = 60
        maxNoM = 65
        availability = 0.7
    elif(step == 16):
        minNoM=25
        maxNoM=30
    elif(step == 19):
        minNoM = 30
        maxNoM = 35
        availability = 0.91
    elif(step == 22):
        minNoM = 45
        maxNoM = 50
        availability = 0.84
    elif(step == 25):
        minNoM = 55
        maxNoM = 60
        availability = 0.8
    elif(step == 28):
        minNoM = 60
        maxNoM = 65
        availability = 0.76
    elif(step == 31):
        minNoM = 70
        maxNoM = 75
        availability = 0.7
    elif(step == 34):
        minNoM=25
        maxNoM=30
    elif(step == 37):
        minNoM = 30
        maxNoM = 35
        availability = 0.91
    elif(step == 40):
        minNoM = 35
        maxNoM = 40
        availability = 0.84
    elif(step == 43):
        minNoM = 45
        maxNoM = 50
        availability = 0.8
    elif(step == 46):
        minNoM = 50
        maxNoM = 55
        availability = 0.76
    elif(step == 49):
        minNoM = 60
        maxNoM = 65
        availability = 0.7
    elif(step == 52):
        minNoM=25
        maxNoM=30


#workload variation
def sim_scenario1(step):
    global maxNoM,minNoM,availability
    if(step == 200):
        minNoM = 30
        maxNoM = 35
        availability = 0.91
    elif(step == 230):
        minNoM = 35
        maxNoM = 40
        availability = 0.9
    elif(step == 260):
        minNoM = 50
        maxNoM = 60
        availability = 0.89
    elif(step == 290):
        minNoM = 60
        maxNoM = 75
        availability = 0.86
    elif(step == 310):
    	minNoM = 80
        maxNoM = 85
        availability = 0.84
    # elif(step == 250):
    # 	minNoM = 40
    #     maxNoM = 45
    #     availability = 0.86
    # elif(step == 260):
    # 	minNoM = 30
    #     maxNoM = 35
    #     availability = 0.89
    # elif(step == 270):
    #     minNoM = 20
    #     maxNoM = 25
    #     availability = 0.93

#Number of participants increases and punctuality drops
def sim_scenario2(step):
    global minNoP,maxNoP,punctuality
    if(step == 200):
        minNoP = 15
        maxNoP = 20
        punctuality = 0.7
    elif(step == 210):
        minNoP = 20
        maxNoP = 25
        punctuality = 0.5
    elif(step == 220):
        minNoP = 25
        maxNoP = 30
        punctuality = 0.4
    elif(step == 230):
        minNoP = 30
        maxNoP = 35
        punctuality = 0.3
    elif(step == 240):
    	minNoP = 35
        maxNoP = 40
        punctuality = 0.2
    elif(step == 250):
    	minNoP = 30
        maxNoP = 35
        punctuality = 0.3
    elif(step == 260):
    	minNoP = 25
        maxNoP = 30
        punctuality = 0.4
    elif(step == 270):
        minNoP = 20
        maxNoP = 25
        punctuality = 0.5

def run():
    global totalMeetingsRequested,maxNoM,totalRoomFound,FhM,NoR,MCA,RfM,HfM,VP2
    global workload, dailyWorkload
    w = np.loadtxt(open("input.csv","rb"),delimiter=",")
    if(controllerON):
        address = ('localhost', 6000)
        print "Connecting to server..."
        conn = Client(address, authkey='secret password')
        print "Connected!"

    for i in xrange(0, SimulationLength):

        if controllerON:
            sim_scenario1d(i)

        else:
            sim_id2(i)

        dailyWorkload = map(int, w[i%60])

        bookMeetings()
        #print dailyWorkload
        totalMeetingsRequested += sum(dailyWorkload)
        resetWorkload()
        calculateI(i+1)
        if(controllerON):
            conn.send([I1[-1],I2[-1],I3[-1],I4[-1]])
            control_input = conn.recv()
            #print 'control input', control_input
            FhM=int(control_input[0])
            NoR=int(control_input[1])
            MCA=int(control_input[2])
            RfM=int(control_input[3])
            HfM=int(control_input[4])
            VP2=int(control_input[5])
        ut.progress(i,SimulationLength)
    if(controllerON):
        conn.send('close')
    ut.log(FhM_log,NoR_log,MCA_log,RfM_log,HfM_log,VP2_log,I1,I2,I3,I4,SimulationLength)
    ut.printI(I1,I2,I3,I4,SimulationLength)
    ut.printSim(FhM_log,NoR_log,MCA_log,RfM_log,HfM_log,VP2_log,SimulationLength)
    plt.show()
    return;

def main():
    run()

if __name__ == "__main__":
    sys.exit(main())

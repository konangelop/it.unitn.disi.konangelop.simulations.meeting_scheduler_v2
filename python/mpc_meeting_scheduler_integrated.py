#!/usr/bin/env python
import numpy as np
import argparse
import os
import sys
import csv

from multiprocessing.connection import Listener
from array import array

#from numpy import *

# My libraries
import libs.mpyc as reg
import libs.utils as ut


def main():
    ## Manage command line inputs
    # Defining command line options to find out the algorithm
    parser = argparse.ArgumentParser( \
        description='Run MPC simulator.', \
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('--simTime',
        type = int,
        help = 'Simulation time.',
        default = 500)

    parser.add_argument('--predictionHorizon',
        type = int,
        help = 'Prediction horizon.',
        default = 10)

    parser.add_argument('--wo',
        type = float,
        help = 'Output weights.',
        default = 1.0)

    parser.add_argument('--wdu',
        type = float,
        help = 'Delta U weights.',
        default = 1.0)

    parser.add_argument('--var_noise',
        type = float,
        help = 'Noise acting on the system output.',
        default = 0.1)

    parser.add_argument('--optim',
        type = int,
        help = 'Online optimization.',
        default = 1)

    parser.add_argument('--fast',
        type = int,
        help = 'Fast version.',
        default = 0)

    parser.add_argument('--timeVarying',
        type = int,
        help = 'Time-varying Kalman filter.',
        default = 1)

    # Parsing the command line inputs
    args = parser.parse_args()

    # System matrices
    A = np.matrix([[0.999846,-0.000704455,0.000145153,3.86579e-06],[-0.000897101,0.992223,-0.00203303,-0.000291882],[0.00168233,0.00219182,0.99715,-6.74425e-05],[-0.00237678,0.00707092,-0.00728359,0.998432]]);
    B = np.matrix([[1.1314e-08,6.91818e-08,-3.31165e-09,1.22676e-08,-5.27818e-10,-1.08599e-07],[3.87309e-08,-1.85485e-07,-9.19101e-09,3.21099e-07,1.28628e-07,-4.70649e-06],[-1.77842e-07,-1.26165e-07,5.98457e-08,2.18785e-08,-2.92153e-07,2.06807e-06],[3.00081e-07,-5.2138e-06,-8.23608e-08,6.63331e-07,2.59505e-06,-2.67676e-05]]);
    C = np.matrix([[5800.22,6.21752,440.363,6.07708],[5888.44,-297.606,397.462,115.905],[7130.7,-424.492,535.736,75.3145],[5060.65,-320.653,70.3568,34.0464]]);
    D = np.matrix([[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0]]);

    # Control parameters
    L = 30;
    Q = np.diag(np.tile(np.matrix([[30,10,10,3]]),[1,L]).tolist()[0]);
    R = np.diag(np.tile(np.matrix([[0.1,0.3,0.5,0.1,2.2,10]]),[1,L]).tolist()[0]);

    # Control saturations
    Umin = np.matrix([[30],[1],[0],[0],[0],[0]]);
    Umax = np.matrix([[100],[5],[100],[40],[30],[1]]);
    DeltaUmin = np.matrix([[-70],[-4],[-100],[-40],[-30],[-1]]);
    DeltaUmax = np.matrix([[70],[4],[100],[40],[30],[1]]);

    # Kalman filter matrices
    Qn = np.diag(np.matrix([[1,1,1,1,1,1]]).tolist()[0]);
    Rn = np.diag(np.matrix([[1,1,1,1]]).tolist()[0]);
    Lk = np.matrix([[1.13449e-07,-6.35313e-08,3.08507e-08,3.31462e-08],[-1.57817e-07,4.96892e-07,-9.30573e-08,-1.09729e-07],[5.51559e-07,-5.1028e-07,1.54073e-07,1.53487e-07],[-1.25535e-05,1.10006e-05,-3.27181e-06,-3.71486e-06]]);
    Pk = np.matrix([[1.63906e-11,-2.24748e-13,6.38223e-11,-1.59791e-09],[-2.24748e-13,1.20984e-09,-5.08947e-10,9.20561e-09],[6.38223e-11,-5.08947e-10,5.73859e-10,-1.09398e-08],[-1.59791e-09,9.20561e-09,-1.09398e-08,2.37444e-07]]);

    # Setpoint
    sp = np.matrix([[80],[100],[100],[80]]);

    # Regulator
    mpc = reg.MPCController(A,B,C,D,\
                            L,Q,R,\
                            Lk,Pk,Qn,Rn,\
                            Umin,Umax,DeltaUmin,DeltaUmax,\
                            optim=bool(args.optim),\
                            fast=bool(args.fast),\
                            time_varying=bool(args.timeVarying))

    ## Simulation loop
    Tfin =1500 #args.simTime
    var_noise = args.var_noise

    # Auxiliary vectors
    p = C.shape[0]
    m = B.shape[1]
    yy  = np.zeros((p,1))
    uu  = np.zeros((m,1))

    sv=0


    address = ('localhost', 6000)     # family is deduced to be 'AF_INET'
    listener = Listener(address, authkey='secret password')
    print "Waiting for connections..."
    conn = listener.accept()
    print "Connection established!"

    while True:
        print "Waiting for data..."
        msg = conn.recv()
        print "Data received!"

        # do something with msg
        if msg == 'close':
            conn.close()
            break
        else:
            #control_input = [float(i) for i in msg[0]]
            yy = np.matrix(msg).T
            #print yy

            # Control law
            uu = mpc.compute_u(yy,sp)
            uu = np.round(uu)
            print 'sending control input: ',uu.T.tolist()[0]
            conn.send(uu.T.tolist()[0])

    # Closing the listener
    listener.close()

    print '\nSimulation completed!\n'


if __name__ == "__main__":
    sys.exit(main())
